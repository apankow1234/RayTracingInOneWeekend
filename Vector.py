import math
    
def isScalar(other):
    if isinstance(other, (int,long,float,complex)) or str(other).isdigit():
        return True
    return False

class Vector(object) :
    
    def __init__(self, *args):
        c = list(args[0]) if len(args) is 1 and isinstance(args[0], (list,tuple)) else list(args)
        self.e = [ float(x) for x in c ]
        
    @property
    def order(self): return len(self.e)
    @property
    def len(self): return abs(self)
    @property
    def unit(self): return self / self.len
    @property
    def x(self): return self.e[0]
    @property
    def y(self): return self.e[1]
    @property
    def z(self): return self.e[2]
    @property
    def r(self): return self.e[0]
    @property
    def g(self): return self.e[1]
    @property
    def b(self): return self.e[2]
    
    def __getitem__(self,i):
        return self.e[i]
    
    def __len__(self):
        return len(self.e)
    
    def isSimilarVector(self, other):
        if isinstance(other, (Vector,list,tuple)) and len(other) == self.order:
            return True
        return False
    
    def __pos__(self): return self
    
    def __neg__(self): return -1 * self
    
    def __add__(self, other):
        if isScalar(other):
            return Vector( [ self[i] + float(other) for i in range(self.order) ] )
        elif self.isSimilarVector(other):
            return Vector( [self[i] + other[i] for i in range(self.order)] )
    def __radd__(self, other):
        return self.__add__(other)
    def __iadd__(self, other):
        if isScalar(other):
            for i in range(self.order):
                self.e[i] += float(other)
        elif self.isSimilarVector(other):
            self.e = [ self[i] + float(other[i]) for i in range(self.order) ]
        return self
    
    def __sub__(self, other):
        if isScalar(other):
            return Vector( [ self[i] - float(other) for i in range(self.order) ] )
        elif self.isSimilarVector(other):
            return Vector( [self[i] - other[i] for i in range(self.order)] )
    def __rsub__(self, other):
        if isScalar(other):
            return Vector( [ float(other) - self[i] for i in range(self.order) ] )
        elif self.isSimilarVector(other):
            return Vector( [float(other[i]) - self[i] for i in range(self.order)] )
    def __isub__(self, other):
        if isScalar(other):
            for i in range(self.order):
                self.e[i] -= float(other)
        elif self.isSimilarVector(other):
            self.e = [ self[i] - float(other[i]) for i in range(self.order) ]
        return self
    
    def __mul__(self, other):
        if isScalar(other):
            return Vector( [ self[i] * float(other) for i in range(self.order) ] )
        elif self.isSimilarVector(other):
            return Vector( [self[i] * other[i] for i in range(self.order)] )
    def __rmul__(self, other):
        return self.__mul__(other)
    def __imul__(self, other):
        if isScalar(other):
            for i in range(self.order):
                self.e[i] *= float(other)
        elif self.isSimilarVector(other):
            self.e = [ self[i] * float(other[i]) for i in range(self.order) ]
        return self
    
    def __div__(self, other):
        if isScalar(other):
            return Vector( [ self[i] / float(other) for i in range(self.order) ] )
        elif self.isSimilarVector(other):
            return Vector( [self[i] / other[i] for i in range(self.order)] )
    def __rdiv__(self, other):
        if isScalar(other):
            return Vector( [ float(other) / self[i] for i in range(self.order) ] )
        elif self.isSimilarVector(other):
            return Vector( [float(other[i]) / self[i] for i in range(self.order)] )
    def __idiv__(self, other):
        if isScalar(other):
            for i in range(self.order):
                self.e[i] /= float(other)
        elif self.isSimilarVector(other):
            self.e = [ self[i] / float(other[i]) for i in range(self.order) ]
        return self
    
    def dot(self, other):
        if self.isSimilarVector(other):
            return sum([ self[i]*other[i] for i in range(self.order) ])
    
    def to(self, other):
        return math.sqrt(self.dot(other))
    
    def cross(self, other):
        nx = self[1]*other[2] - self[2]*other[1]
        ny = -(self[0]*other[2] - self[2]*other[0])
        nz = self[0]*other[1] - self[1]*other[0]
        return Vector(nx, ny, nz)
    
    def __abs__(self):
        return self.to(self)
    
    def __str__(self):
        return '('+', '.join( [str(x) for x in self.e] )+')'

        
#------------------------------------------------------------ v1 = Vector(1,2,3)
#---------------------------------------------------------- v2 = Vector([4,5,6])
#------------------------------------------------------------------ print v1, v2
#---------------------------------------------------------------------- v1 += v2
#------------------------------------------------------------------ print v1, v2
#------------------------------------------------------------------------------ 
#------------------------------------------------------------------ print v1.len
#----------------------------------------------------------------- print v1.unit
#------------------------------------------------------------ print v1.cross(v2)
#--------------------------------------------------------------------- print -v2
