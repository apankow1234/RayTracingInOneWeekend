from Vector import *

class Ray :
    
    def __init__(self, a, b, ti=0.0):
        if len(a) == len(b) == 3 :
			self.time = ti
            self.origin = a if isinstance(a, Vector) else Vector(a)
            self.direction = b if isinstance(b, Vector) else Vector(b)
        
    def point_at_parameter(self, t):
        # point along a ray p(t)=A+tB
        return self.origin + t * self.direction