import random
from Ray import *

def random_in_unit_disk():
    p = Vector(1,1,0)
    while True :
        p = 2.0 * Vector(random.random(),random.random(), 0) - Vector(1,1,0)
        if p.dot(p) >= 1.0 :
            break
    return p

class Camera:
    
    def __init__(self, lookfrom, lookat, vup, vfov, aspect, aperture, focus_dist): #field of view is top to bottom in degrees
        self.lens_radius = aperture/2
        theta = vfov * math.pi / 180 #theta is field of view in radians
        half_height = math.tan(theta / 2.0)
        half_width = aspect * half_height
        self.origin = lookfrom
        self.w = (lookfrom - lookat).unit
        self.u = (vup.cross(self.w)).unit
        self.v = self.w.cross(self.u)
        #---------- self.lower_left_corner = Vector(-half_width,-half_height,-1)
        self.lower_left_corner = self.origin-half_width*focus_dist*self.u-half_height*focus_dist*self.v-focus_dist*self.w
        self.horizontal = 2.0*half_width*self.u*focus_dist
        self.vertical = 2.0*half_height*self.v*focus_dist
        
    def get_ray(self, s, t):
        rd = self.lens_radius*random_in_unit_disk()
        offset = self.u*rd.x+self.v*rd.y
        return Ray( self.origin+offset, self.lower_left_corner+s*self.horizontal+t*self.vertical-self.origin-offset )