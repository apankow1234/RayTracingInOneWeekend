
from Hittable import *

class Lambertian(Material):
    
    def __init__(self, a):
        Material.__init__(self)
        self.albedo = a
    
    def scatter(self, r_in, rec):
        target = rec.p + rec.normal + random_in_unit_sphere()
        Material.scattered = Ray(rec.p, target - rec.p)
        Material.attenuation = self.albedo
        return True
    
Default = Lambertian(Vector(.5,.5,.5)).id