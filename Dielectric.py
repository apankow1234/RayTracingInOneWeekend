from Hittable import *

class Dielectric(Material):
    
    def __init__(self, ri):
        Material.__init__(self)
        self.ref_idx = float(ri)
        
    def scatter(self, r_in, rec):
        outward_normal = Vector(0,0,0)
        ni_over_nt = float()
        reflected = reflect(r_in.direction, rec.normal)
        Material.attenuation = Vector(1,1,1)
        reflect_prob = float()
        cosine = float()
        
        if r_in.direction.dot(rec.normal) > 0:
            outward_normal = -rec.normal
            ni_over_nt = self.ref_idx
            cosine = self.ref_idx * r_in.direction.dot(rec.normal)/abs(r_in.direction)
        else:
            outward_normal = rec.normal
            ni_over_nt = 1.0 / self.ref_idx
            cosine = -r_in.direction.dot(rec.normal)/abs(r_in.direction)
            
        Material.refracted = refract(r_in.direction, outward_normal, ni_over_nt)
        if(self.refracted):
            reflect_prob = schlick(cosine, self.ref_idx)
        else:
            reflect_prob = 1.0
        
        if random.random() < reflect_prob:
            self.scattered = Ray(rec.p, reflected)
        else:
            self.scattered = Ray(rec.p, self.refracted)
        return True