from Hittable import *

class SphereHittable(Hittable):
    
    def __init__(self, center, radius, mat_id=0):
        Hittable.__init__(self)
        self.rec.mat_id = mat_id
        if isinstance(center, Vector) and isScalar(radius):
            self.center = center
            self.radius = float(radius)
    
    def hit(self, r, t_min, t_max):
        if isinstance(r, Ray):
            oc = r.origin - self.center
            a = r.direction.dot(r.direction)
            b = 2.0*oc.dot(r.direction)
            c = oc.dot(oc) - self.radius**2
            discriminant = b*b - 4*a*c
            if discriminant > 0:
                temp = (-b - math.sqrt(discriminant))/(2*a)
                if t_min < temp < t_max:
                    self.rec.t = temp
                    self.rec.p = r.point_at_parameter(self.rec.t)
                    self.rec.normal = (self.rec.p - self.center)/self.radius
                    #------------------------------------- print self.rec.normal
                    return True
                temp = (-b + math.sqrt(discriminant))/(2*a)
                if t_min < temp < t_max:
                    self.rec.t = temp
                    self.rec.p = r.point_at_parameter(self.rec.t)
                    self.rec.normal = (self.rec.p - self.center)/self.radius
                    #------------------------------------- print self.rec.normal
                    return True
        return False