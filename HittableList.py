from Hittable import *

class HittableList(Hittable):
    
    def __init__(self, *args):
        Hittable.__init__(self)
        self.list = args[0] if len(args)==1 and isinstance(args[0], (list, tuple)) else list(args)
    
    def hit(self, r, t_min, t_max):
        hit_anything = False
        closest_so_far = t_max
        if isinstance(r, Ray):
            for hittable in self.list:
                if hittable.hit(r, t_min, closest_so_far):
                    #--------------------------------- print hittable.rec.normal
                    hit_anything = True
                    closest_so_far = hittable.rec.t
                    self.rec = hittable.rec
                    # print self.rec.normal, hittable.rec.normal, self.rec.normal==hittable.rec.normal
        return hit_anything
