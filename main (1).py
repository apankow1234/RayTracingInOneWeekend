import os, sys, random, datetime
# from random import random
from SphereHittable import *
from HittableList import *
from Camera import *
from Lambertian import *
from Metal import *
from Dielectric import *

def last_render(render_dir, ext, render_filename,frame_num):
    tries = [0]
    for root, dirs, files in os.walk(render_dir):
        for file in [x for x in files if x.endswith('.'+ext) and x.startswith(render_filename)] :
            p = file.split('.')
            if frame_num == int( p[1] ) and p[2].isdigit():
                tries.append( int(p[2]) )
    return max(tries)

def color_at(r, world, depth = 0):
    if world.hit(r, 0.0, sys.float_info.max):
        mat = Material.get(world.rec.mat_id)
        if depth < 50 and mat.scatter(r, world.rec):
            return mat.attenuation * color_at(mat.scattered, world, depth+1)
        else:
            return Vector(0,0,0)
    else:
        t = 0.5 * r.direction.unit.y + 1.0
        return ( 1.0 - t ) * Vector(1,1,1) + t * Vector(0.5,0.7,1) #a light blue-to-white vertical gradient bg
        #---------------------------------------- return Vector(1,1,1) #white bg

def random_scene():
	a = 500
	hittable_list = []
	hittable_list.append(SphereHittable(Vector(0,-1000,0), 1000))
	for a in range(-11,11):
		for b in range(-11,11):
			choose_mat = random.random()
			center = Vector(a+0.9*random.random(),0.2, b+0.9*random.random())
			# print center
			if (center - Vector(4, 0.2,0)).len > 0.9:
				# print center
				if choose_mat < 0.8:
					# print 'lambert'
					lambert = Lambertian(Vector(random.random()*random.random(),random.random()*random.random(),random.random()*random.random())).id
					hittable_list.append(SphereHittable(center,0.2, lambert))
				elif choose_mat < 0.95:
					# print 'metal'
					metallic = Metal(Vector(0.5*(1+random.random()), 0.5*(1+random.random()), 0.5*(1+random.random())), 0.5*(1+random.random())).id
					hittable_list.append(SphereHittable(center,0.2, metallic))
				else:
					# print 'dielectric'
					hittable_list.append(SphereHittable(center,0.2, Dielectric(1.5).id))
	hittable_list.append(SphereHittable(Vector(0,1,0), 1, Dielectric(1.5).id))
	hittable_list.append(SphereHittable(Vector(-4,1,0), 1, Lambertian(Vector(0.4,0.2,0.1)).id))
	hittable_list.append(SphereHittable(Vector(4,1,0), 1, Metal(Vector(0.7,0.6,0.5),0.0).id))
	# print hittable_list
	return HittableList(hittable_list)


# Render Settings
nx = 200
ny = 100
ns = 100

# Render File
filename = 'book1_finale'
ext = 'ppm'
header = 'P3\n%d %d\n255\n'%(nx,ny)
render_dir = 'renders'
render_path = os.path.dirname(os.path.realpath(__file__))+os.sep+render_dir
frame_num = 1
this_try = last_render(render_dir, ext, filename, frame_num)+1
full_name = '.'.join( [filename,str(frame_num),str(this_try),ext] )

#Scene
start_frame = 1
# # ------------------------------------- RedDiffuse = Lambertian(Vector(1,0,0)).id
# # ------------------------------------ BlueDiffuse = Lambertian(Vector(0,0,1)).id
Lambert1 = Lambertian(Vector(.8,.8,0)).id
Lambert2 = Lambertian(Vector(.8,.3,.3)).id
Metal1 = Metal(Vector(.7,.7,.25), 1).id
Metal2 = Metal(Vector(1,1,1), 0).id
Dielectric1 = Dielectric(1.5).id
SceneList=[]
SceneList.append(SphereHittable(Vector(0,-1000,-1), 1000, Lambert1))
SceneList.append(SphereHittable(Vector(0,0,-1), 0.5 ))
SceneList.append(SphereHittable(Vector(1,0,-1), 0.5))
# SceneList.append(SphereHittable(Vector(1,0,-1), -0.45, Dielectric1))
SceneList.append(SphereHittable(Vector(-1,0,-1), 0.5))
# ----------------------------------------------------- R = math.cos(math.pi / 4)
# # ------------- SceneList.append(SphereHittable(Vector(-R,0,-1), R, BlueDiffuse))
# # --------------- SceneList.append(SphereHittable(Vector(R,0,-1), R, RedDiffuse))
#----------------------------------------------- world = HittableList(SceneList)
world = random_scene()
# print world

render_file = open( render_path+os.sep+full_name, 'w' )
render_file.write( header )
render_file.close()
startTime = datetime.datetime.now()
print full_name
print '   begin: ', startTime
render_file = open( render_path+os.sep+full_name, 'a' )

lookfrom = Vector(3,.1,3)
lookat = Vector(0,0,-1)
dist_to_focus = (lookfrom-lookat).len
aperture = .5
cam = Camera(lookfrom,lookat,Vector(0,1,0),25, nx/ny, aperture, dist_to_focus)

ctr = 0
for j in reversed( xrange( ny ) ) :
    for i in range( nx ) :
        ctr += 1
        col = Vector(0,0,0)
        for s in range(ns):
            u = float(i + (random.random() if ns > 1 else 0) ) / float(nx)
            v = float(j + (random.random() if ns > 1 else 0) ) / float(ny)
            r = cam.get_ray(u,v)
            p = r.point_at_parameter(2.0)
            col += color_at(r, world)
        col /= float(ns)
        newCol = Vector(int(255.99 * math.sqrt(col.r)),int(255.99 * math.sqrt(col.g)),int(255.99 * math.sqrt(col.b)))
        render_file.write( '%d %d %d\n' %( newCol.r, newCol.g, newCol.b ) )

        pr = float(ctr)/float(nx*ny)*100
        if pr%10 == 0: # print out every 10% complete
            print '      '+str(pr)+'%'
render_file.close()
endTime = datetime.datetime.now()
renderTime = endTime - startTime
print '   done: ', endTime
print 'time: ', renderTime
