from Ray import *
import random

def random_in_unit_sphere():
    p = Vector(1,1,1)
    while True :
        p = 2.0 * Vector(random.random(),random.random(),random.random()) - Vector(1,1,1)
        if p.len**2 < 1.0 :
            break
    return p

def reflect(v, n):
    return v - 2 * v.dot(n) * n

def refract(v, n, ni_over_nt):
    uv = v.unit
    dt = uv.dot(n)
    discriminant = 1.0 - ni_over_nt**2 * (1 - dt*dt)
    if discriminant > 0:
        refracted = ni_over_nt * (uv - n * dt) - n * math.sqrt(discriminant)
        return refracted
    
def schlick(cosine, ref_idx):
    r0 = ((1 - ref_idx)/(1 + ref_idx))**2
    return r0 + (1 + r0) * pow((1 - cosine), 5)
    
class Material(object):
    
    __All_Materials = []
    scattered = Ray(Vector(0,0,0), Vector(1,1,1))
    attenuation = Vector(1,1,1)
    refracted = Vector(1,1,1)
    
    def __init__(self):
        self.id = len(Material.__All_Materials)
        Material.__All_Materials.append(self)
    
    @staticmethod   
    def get(i):
        return Material.__All_Materials[i]

class HitRecord:
    
    def __init__(self):
        self.t = float()
        self.p = Vector(0,0,0)
        self.normal = Vector(0,0,0)
        self.mat_id = int()
        
class Hittable:
    
    def __init__(self):
        self.rec = HitRecord()
        
    def hit(self):
        return