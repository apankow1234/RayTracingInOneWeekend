from Hittable import *


class Metal(Material):
    
    def __init__(self, a, f):
        Material.__init__(self)
        self.albedo = a
        self.fuzz = f
        
    def scatter(self, r_in, rec):
        reflected = reflect(r_in.direction.unit, rec.normal)
        Material.scattered = Ray(rec.p, reflected + self.fuzz * random_in_unit_sphere())
        Material.attenuation = self.albedo
        return Material.scattered.direction.dot(rec.normal) > 0