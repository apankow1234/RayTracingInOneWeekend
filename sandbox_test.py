from Vector import *

def test(v):
    v.list = [4,5,6]
    return True

v1 = Vector(1,2,3)
print v1
print test(v1)
print v1


def addToList(theList):
    theList.append(3)
    theList.append(4)

def addToNewList(theList):
    theList = list()
    theList.append(3)
    theList.append(4)

myList = list()
myList.append(1)
myList.append(2)
addToList(myList)
print(myList)
addToNewList(myList)
print(myList)